var map = L.map('main_map').setView([19.431847, -99.132989], 11); //coordenadas iniciales mapa

var greenIcon = L.icon({
    iconUrl: 'leaf-green.png',
    shadowUrl: 'leaf-shadow.png',

    iconSize:     [38, 95], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    atribution: '&copy; <a href="https://www.opemstreetmap.org/copyright">OpenStreetMap</a> contributors',
    accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwODA3NGJiMDljMzEzMDJmODc5ZTJjMCIsImlhdCI6MTYxOTAzMTMwNiwiZXhwIjoxNjE5NjM2MTA2fQ.4GAZ6gDSnSkHkJdnMBRo7DcGJP56bVsTUoTwcx5JFD8'
}).addTo(map);

var headers = {'content-type': 'application/json'};
var body = JSON.stringify({ "email":"andres@correo.com", "password":"password" });


$.ajax({
    type: "POST",
    headers: headers,
    url: "api/auth/authenticate",
    data: body, 
    success: function(res) {
        headers = {'Content-Type': 'application/json', 'x-access-token': res.data.token};
        $.ajax({
            headers: headers,
            url: "api/arboles",
            type: "GET",
            dataType: "json",
            success: function(res) {
                res.arboles.forEach(function(arbolito) {
                    var customIcon = new L.Icon({
                        iconUrl: '../images/img/arbol.svg',
                        iconSize: [50, 50],
                        iconAnchor: [25, 50]
                      });
                    titulo= "Identificador de árbol: "+arbolito._id;
                    ubicacion= [arbolito.latitud, arbolito.longitud];
                    var marker = L.marker(ubicacion, {icon:customIcon});
                    alcaldia = "<b>Alcaldia</b>: "+arbolito.alcaldia;
                    estado = "Estado: "+arbolito.estado.nomEstado;
                    direccion= "<b>Dirección: </b> "+arbolito.direccion;
                    coordenadas= "<b>Coordenadas: </b> "+ ubicacion;
                    emplazamiento= "<b>Emplazamiento: </b> "+arbolito.emplazamiento.nomEmplazamiento;
                    nomEspecie= "<b>Nombre: </b> "+arbolito.especie.nomEspecie;
                    nomCientifico= "<b>N. Cientifico: </b> "+arbolito.especie.nomCientifico;
                    familiaEspecie= "<b>Familia: </b> "+arbolito.especie.familiaEspecie;
                    formaCopa = "<b>Forma de la copa: </b> "+arbolito.especie.formaCopa;
                    formaFruto = "<b>Forma del fruto: </b> "+arbolito.especie.formaFruto;
                    colorFruto = "<b>Color del fruto: </b> "+arbolito.especie.colorFruto;
                    colorCortezaExt = "<b>Color de la corteza externa: </b> "+arbolito.especie.colorCortezaExt;
                    colorCortezaInt = "<b>Color de la corteza interna: </b> "+arbolito.especie.colorCortezaInt;
                    caracTronco = "<b>Caracteristicas del tronco: </b> "+arbolito.especie.caracTronco;
                    colorHoja = "<b>Color de la hoja: </b> "+arbolito.especie.colorHoja;
                    tamAproxHoja = "<b>Tamaño aproximado de la hoja: </b> "+arbolito.especie.tamAproxHoja;
                    formaHoja = "<b>Forma de la hoja: </b> "+arbolito.especie.formaHoja;
                    alturaMaxEspecie = "<b>Altura máxima: </b> "+arbolito.especie.alturaMaxEspecie;
                    aAltura = "<b>Altura actual aprox.: </b> "+arbolito.aAltura;
                    diametroDAP= "<b>Diametro al pecho máximo: </b> "+arbolito.especie.diametroDAP;
                    aDiametro= "<b>Diametro al pecho actual: </b> "+arbolito.aDiametro;
                    nomEstado = "<b>Nombre: </b> "+arbolito.estado.nomEstado;
                    descripcion= "<b>Descripción: </b> "+arbolito.estado.descripcion;
                    marker.bindPopup(`<b>${titulo}</b><br><br><b>UBICACIÓN</b><br>
                                    ${alcaldia}<br>${direccion}<br>${coordenadas}<br>
                                    ${emplazamiento}<br><br><b>ESPECIE</b><br>${nomEspecie}<br>
                                    ${nomCientifico}<br>${familiaEspecie}<br>${formaCopa}
                                    <br><br><b>Datos de los frutos</b><br>${formaFruto}<br>${colorFruto}
                                    <br><br><b>Datos del tronco</b><br>${colorCortezaExt}<br>${colorCortezaInt}
                                    <br>${caracTronco}<br><br><b>Datos de las hojas</b><br>${colorHoja}
                                    <br>${tamAproxHoja}<br>${formaHoja}<br><br><b>ALTURA</b><br>${alturaMaxEspecie}metros<br>${aAltura}metros
                                    <br><b>DIAMETRO DEL TRONCO</b><br>${diametroDAP}metros
                                    <br>${aDiametro}metros
                                    <br><br><b>ESTADO DEL ÁRBOL</b><br>
                                    ${nomEstado}<br>${descripcion}`);
                    marker.addTo(map);
                    
                    //market.addTo(map);
                });
                console.log('Ubicación de los arboles cargados en el mapa con exito!...');
            }, 
            error: function(err) {
                console.log(err);
            }
        });
    },
    error: function(err) {
        console.log(err);
    }
});
