var Arbol = require('../../models/arbol');
var request = require('request');
var server = require('../../bin/www');


describe('Arbol API', ()=>{
    describe('GET ARBOLES /', () => {
        it('Status 200', () => {
            expect(Arbol.allArboles.length).toBe(0);

            var a = new Arbol(1, 'negro', 'urbana', [-34.6012424, -58.3861497]);
            Arbol.add(a);

            request.get('http://localhost:3001/api/arboles', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST ARBOLES /create', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var unArbol = '{"id":10, "color": "rojo", "modelo": "urbana", "lat":-34, "lng":-54}';
            request.post({
                headers: headers,
                url: 'http://localhost:3001/api/arboles/create',
                body: unArbol
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Arbol.findById(10).color).toBe("rojo");
                done();
            });
        });
    });
});

    
    
