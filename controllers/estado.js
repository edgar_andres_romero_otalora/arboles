var Estado = require('../models/estado');

exports.estado_list = function (req, res) {
    Estado.allEstados(function (err, estados) {
        res.render('estados/index', { estaditos: estados });
    });
}

exports.estado_lista = function (req, res) {
    Estado.allEstados(function (err, estados) {
        res.render('arboles/listaEstado', { estaditos: estados });
    });
}

exports.estado_create_get = function (req, res) {
    res.render('estados/create');
}

exports.estado_create_post = function (req, res) {
    var estadito = new Estado(
        {
            code: req.body.code,
            nomEstado: req.body.nomEstado,
            descripcion: req.body.descripcion
        }
    );
    console.log("estado a añadir", estadito);
    Estado.add(estadito);
    res.redirect('/estados');
}

exports.estado_update_get = function (req, res) {
    console.log("req.params", req.params)
    Estado.findById(req.params.id).exec((err, estadito) => {
        res.render('estados/update', {
            estadito
        });
    })
}

exports.estado_update_post = function (req, res) {
    var update_values = {
        nomEstado: req.body.nomEstado,
        descripcion: req.body.descripcion,
    };
    Estado.findByIdAndUpdate(req.params.id, update_values,
        (err, estado) => {
            if (err) {
                console.log(err);
                res.render('estados/update', {
                    errors: err.errors,
                    estado_create_get
                })
            } else {
                res.redirect('/estados');
                return;
            }
        })
}

exports.estado_delete_post = function (req, res) {
    Estado.findByIdAndDelete(req.body.id, (err) => {
        if (err) {
            next(err);
        } else {
            res.redirect('/estados');
        }
    });
}