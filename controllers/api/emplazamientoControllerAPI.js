var Emplazamiento = require('../../models/emplazamiento');

exports.emplazamiento_list = function (req, res) {
    Emplazamiento.allEmplazamientos(function (err, esplazamienticos) {
        if (err) console.log(err);
        res.status(200).json({
            emplazamientos: esplazamienticos
        });
    });
};

exports.emplazamiento_lista = function (req, res) {
    Emplazamiento.allEmplazamientos(function (err, emplazamienticos) {
        if (err) console.log(err);
        res.status(200).json({
            emplazamientos: esplazamienticos
        });
    });
};

exports.emplazamiento_create = function(req, res) {
    var esplazamientico = Emplazamiento.createInstance(req.body.code, req.body.nomEmplazamiento);
    Emplazamiento.add(esplazamientico, function(err, newEmplazamiento) { 
      if (err) console.log(err);
      res.status(200).json({
        emplazamiento: newEmplazamiento
      });
    });
  };

exports.emplazamiento_update = function (req, res) {
    var esplazamientico = Emplazamiento.findById(req.params.id);
    esplazamientico.nomEspecie = req.body.nomEmplazamiento;   

    res.status(200).json({
        emplazamiento: esplazamientico
    });
}

exports.emplazamiento_delete = function (req, res) {
    Emplazamiento.removeById(req.body.id);
    res.status(204).send();
}