var Especie = require('../../models/especie');

exports.especie_list = function (req, res) {
    Especie.allEspecies(function (err, especiecitas) {
        if (err) console.log(err);
        res.status(200).json({
            especies: especiecitas
        });
    });
};

exports.especie_lista = function (req, res) {
    Especie.allEspecies(function (err, especiecitas) {
        if (err) console.log(err);
        res.status(200).json({
            especies: especiecitas
        });
    });
};

exports.especie_create = function(req, res) {
    var especiecita = Especie.createInstance(req.body.code, 
        req.body.nomEspecie, req.body.nomCientifico, 
        req.body.familiaEspecie, req.body.alturaMaxEspecie, diametroDAP, formaCopa, 
        req.body.formaFruto, req.body.colorFruto,
        req.body.colorCortezaExt, req.body.colorCortezaInt, req.body.caracTronco, 
        req.body.colorHoja, req.body.tamAproxHoja, req.body.formaHoja);
    Especie.add(especiecita, function(err, newEspecie) { 
      if (err) console.log(err);
      res.status(200).json({
        especie: newEspecie
      });
    });
  };

exports.especie_update = function (req, res) {
    var especiecita = Especie.findById(req.params.id);
    especiecita.nomEspecie = req.body.nomEspecie;
    especiecita.nomCientifico = req.body.nomCientifico;
    especiecita.familiaEspecie = req.body.familiaEspecie;
    especiecita.alturaMaxEspecie = req.body.alturaMaxEspecie;
    especiecita.formaCopa = req.body.formaCopa,
    arbolito.formaFruto=req.body.formaFruto;
    arbolito.colorFruto=req.body.colorFruto;
    arbolito.colorCortezaExt=req.body.colorCortezaExt;
    arbolito.colorCortezaInt=req.body.colorCortezaInt;
    arbolito.caracTronco=req.body.caracTronco;
    arbolito.colorHoja=req.body.colorHoja;
    arbolito.tamAproxHoja=req.body.tamAproxHoja;
    arbolito.formaHoja=req.body.formaHoja;

    res.status(200).json({
        especie: especiecita
    });
}

exports.especie_delete = function (req, res) {
    Especie.removeById(req.body.id);
    res.status(204).send();
}