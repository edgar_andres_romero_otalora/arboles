var Arbol = require('../../models/arbol');

exports.arbol_list = function (req, res) {
    Arbol.allArboles(function (err, arbolitos) {
        if (err) console.log(err);
        res.status(200).json({
            arboles: arbolitos
        });
    });
};

exports.arbol_create = function(req, res) {
    var arbolito = Arbol.createInstance(req.body.code, req.body.alcaldia, req.body.direccion, 
        req.body.latitud, req.body.longitud, req.body.emplazamiento, req.body.especie, 
        req.body.estado, req.body.aAltura, req.body.aDiametro);
    Arbol.add(arbolito, function(err, newArbol) { 
        if (err) console.log(err);
        res.status(200).json({
          arbol: newArbol
        });
      });
  };

exports.arbol_update = function (req, res) {
    var arbolito = Arbol.findById(req.params.id);
    arbolito.alcaldia = req.body.alcaldia;
    arbolito.direccion = req.body.direccion;
    arbolito.latitud = req.body.latitud;
    arbolito.longitud = req.body.longitud;
    arbolito.emplazamiento = req.body.emplazamiento;
    arbolito.especie = req.body.especie;
    arbolito.estado = req.body.estado;
    arbolito.aAltura= req.body.aAltura;
    arbolito.aDiametro= req.body.aDiametro  
    
    res.status(200).json({
        arbol: arbolito
    });
}

exports.arbol_delete = function (req, res) {
    Arbol.removeById(req.body.id);
    res.status(204).send();
}
