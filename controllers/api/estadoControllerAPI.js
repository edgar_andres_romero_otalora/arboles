var Estado = require('../../models/estado');

exports.estado_list = function (req, res) {
    Estado.allEstado(function (err, estaditos) {
        if (err) console.log(err);
        res.status(200).json({
            estados: estaditos
        });
    });
};

exports.estado_lista = function (req, res) {
    Estado.allEstado(function (err, estaditos) {
        if (err) console.log(err);
        res.status(200).json({
            estados: estaditos
        });
    });
};

exports.estado_create = function(req, res) {
    var estadito = Estado.createInstance(req.body.code, req.body.nomEstado, req.body.descripcion);
    Estado.add(estadito, function(err, newEstado) { 
      if (err) console.log(err);
      res.status(200).json({
        estado: newEstado
      });
    });
  };

exports.estado_update = function (req, res) {
    var estadito = Estado.findById(req.params.id);
    estadito.nomEstado = req.body.nomEstado;
    estadito.descripcion = req.body.descripcion;

    res.status(200).json({
        estado: estadito
    });
}

exports.estado_delete = function (req, res) {
    Estado.removeById(req.body.id);
    res.status(204).send();
}