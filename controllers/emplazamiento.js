var Emplazamiento = require('../models/emplazamiento');

exports.emplazamiento_list = function (req, res) {
    Emplazamiento.allEmplazamientos(function (err, emplazamientos) {
        res.render('emplazamientos/index', { emplazamienticos: emplazamientos });
    });
}

exports.emplazamiento_lista = function (req, res) {
    Emplazamiento.allEmplazamientos(function (err, emplazamientos) {
        res.render('arboles/listaEmplazamiento', { emplazamienticos: emplazamientos });
    });
}

exports.emplazamiento_create_get = function (req, res) {
    res.render('emplazamientos/create');
}

exports.emplazamiento_create_post = function (req, res) {
    var emplazamientico = new Emplazamiento(
        {
            code: req.body.code,
            nomEmplazamiento: req.body.nomEmplazamiento
        }
    );
    console.log("emplazamiento a añadir", emplazamientico);
    Emplazamiento.add(emplazamientico);
    res.redirect('/emplazamientos');
}

exports.emplazamiento_update_get = function (req, res) {
    console.log("req.params", req.params)
    Emplazamiento.findById(req.params.id).exec((err, emplazamientico) => {
        res.render('emplazamientos/update', {
            emplazamientico
        });
    })
}

exports.emplazamiento_update_post = function (req, res) {
    var update_values = {
        nomEmplazamiento: req.body.nomEmplazamiento
    };
    Emplazamiento.findByIdAndUpdate(req.params.id, update_values,
        (err, emplazamiento) => {
            if (err) {
                console.log(err);
                res.render('emplazamientos/update', {
                    errors: err.errors,
                    emplazamiento_create_get
                })
            } else {
                res.redirect('/emplazamientos');
                return;
            }
        })
}

exports.emplazamiento_delete_post = function (req, res) {
    Emplazamiento.findByIdAndDelete(req.body.id, (err) => {
        if (err) {
            next(err);
        } else {
            res.redirect('/emplazamientos');
        }
    });
}