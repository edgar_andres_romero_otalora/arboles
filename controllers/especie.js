var Especie = require('../models/especie');

exports.especie_list = function (req, res) {
    Especie.allEspecies(function (err, especies) {
        res.render('especies/index', { especiecitas: especies });
    });
}

exports.especie_lista = function (req, res) {
    Especie.allEspecies(function (err, especies) {
        res.render('arboles/listaEspecie', { especiecitas: especies });
    });
}

exports.especie_create_get = function (req, res) {
    res.render('especies/create');
}

exports.especie_create_post = function (req, res) {
    var especiecita = new Especie(
        {
            code: req.body.code,
            nomEspecie: req.body.nomEspecie,
            nomCientifico: req.body.nomCientifico,
            familiaEspecie: req.body.familiaEspecie,
            alturaMaxEspecie: req.body.alturaMaxEspecie,
            diametroDAP: req.body.diametroDAP,
            formaCopa: req.body.formaCopa,
            formaFruto:req.body.formaFruto,
            colorFruto:req.body.colorFruto,
            colorCortezaExt:req.body.colorCortezaExt,
            colorCortezaInt:req.body.colorCortezaInt,
            caracTronco:req.body.caracTronco,
            colorHoja:req.body.colorHoja,
            tamAproxHoja:req.body.tamAproxHoja,
            formaHoja:req.body.formaHoja
        }
    );
    console.log("especie a añadir", especiecita);
    Especie.add(especiecita);
    res.redirect('/especies');
}

exports.especie_update_get = function (req, res) {
    console.log("req.params", req.params)
    Especie.findById(req.params.id).exec((err, especiecita) => {
        res.render('especies/update', {
            especiecita
        });
    })
}

exports.especie_update_post = function (req, res) {
    var update_values = {
        nomEspecie: req.body.nomEspecie,
        nomCientifico: req.body.nomCientifico,
        familiaEspecie: req.body.familiaEspecie,
        alturaMaxEspecie: req.body.alturaMaxEspecie,
        diametroDAP: req.body.diametroDAP,
        formaCopa: req.body.formaCopa,
        formaFruto:req.body.formaFruto,
        colorFruto:req.body.colorFruto,
        colorCortezaExt:req.body.colorCortezaExt,
        colorCortezaInt:req.body.colorCortezaInt,
        caracTronco:req.body.caracTronco,
        colorHoja:req.body.colorHoja,
        tamAproxHoja:req.body.tamAproxHoja,
        formaHoja:req.body.formaHoja
    };
    Especie.findByIdAndUpdate(req.params.id, update_values,
        (err, especie) => {
            if (err) {
                console.log(err);
                res.render('especies/update', {
                    errors: err.errors,
                    especie_create_get
                })
            } else {
                res.redirect('/especies');
                return;
            }
        })
}

exports.especie_delete_post = function (req, res) {
    Especie.findByIdAndDelete(req.body.id, (err) => {
        if (err) {
            next(err);
        } else {
            res.redirect('/especies');
        }
    });
}