var express = require('express');
var router = express.Router();
var especieController= require('../../controllers/api/especieControllerAPI');

router.get('/', especieController.especie_list);
router.post('/create', especieController.especie_create);
router.post('/:id/update', especieController.especie_update);
router.delete('/delete', especieController.especie_delete);

module.exports = router;