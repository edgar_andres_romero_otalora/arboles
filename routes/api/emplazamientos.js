var express = require('express');
var router = express.Router();
var emplazamientoController= require('../../controllers/api/emplazamientoControllerAPI');

router.get('/', emplazamientoController.emplazamiento_list);
router.post('/create', emplazamientoController.emplazamiento_create);
router.post('/:id/update', emplazamientoController.emplazamiento_update);
router.delete('/delete', emplazamientoController.emplazamiento_delete);

module.exports = router;