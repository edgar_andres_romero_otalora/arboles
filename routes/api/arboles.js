var express = require('express');
var router = express.Router();
var arbolController= require('../../controllers/api/arbolControllerAPI');
var especieController= require('../../controllers/api/especieControllerAPI');
var emplazamientoController= require('../../controllers/api/emplazamientoControllerAPI');
var estadoController= require('../../controllers/api/estadoControllerAPI');

router.get('/', arbolController.arbol_list);
router.post('/create', arbolController.arbol_create);
router.post('/:id/update', arbolController.arbol_update);
router.delete('/delete', arbolController.arbol_delete);
router.get('/listaEspecie', especieController.especie_lista);
router.get('/listaEmplazamiento', emplazamientoController.emplazamiento_lista);
router.get('/listaEstado', estadoController.estado_lista);

module.exports = router;