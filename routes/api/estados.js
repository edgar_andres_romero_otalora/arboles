var express = require('express');
var router = express.Router();
var estadoController= require('../../controllers/api/estadoControllerAPI');

router.get('/', estadoController.estado_list);
router.post('/create', estadoController.estado_create);
router.post('/:id/update', estadoController.estado_update);
router.delete('/delete', estadoController.estado_delete);

module.exports = router;