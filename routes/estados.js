var express = require('express');
var router = express.Router();
var estadoController= require('../controllers/estado');

router.get('/', estadoController.estado_list);
router.get('/create', estadoController.estado_create_get);
router.post('/create', estadoController.estado_create_post);
router.get('/:id/update', estadoController.estado_update_get);
router.post('/:id/update', estadoController.estado_update_post);
router.post('/:id/delete', estadoController.estado_delete_post);


module.exports = router;