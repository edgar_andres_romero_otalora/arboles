var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var arbolSchema = new mongoose.Schema({
    code: Number,
    alcaldia: String, 
    direccion: String,    
    latitud: Number,
    longitud: Number,
    aAltura: Number,
    aDiametro: Number,
    emplazamiento: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Emplazamiento'
    },
    especie: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Especie'
    },
    estado: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Estado'
    }
});

arbolSchema.statics.createInstance = function (code, alcaldia, direccion, latitud, longitud, 
    emplazamiento, especie, estado, aAltura, aDiametro) {
    return new this({
        code: code,        
        alcaldia:alcaldia,  
        direccion: direccion,  
        latitud: latitud,
        longitud: longitud,
        emplazamiento:emplazamiento,
        especie:especie,
        estado:estado,
        aAltura:aAltura,
        aDiametro:aDiametro
    })
};

arbolSchema.methods.toString = function () {
    return 'code: ' + this.code + ' | color ' + this.color;
};

arbolSchema.statics.allArboles = function (cb) {
    return this.find({}, cb).populate('especie').populate('emplazamiento').populate('estado');
};

arbolSchema.statics.add = function (unArbol, cb) {
    return this.create(unArbol, cb);
};


arbolSchema.statics.findByCode = function (aCode, cb) {
    return this.findOne({ code: aCode }, cb).populate('especie').populate('emplazamiento').populate('estado');
};

arbolSchema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model('Arbol', arbolSchema);
