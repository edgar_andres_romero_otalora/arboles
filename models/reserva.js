var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Arbol y usuario es una referencia de un objeto
var reservaSchema = new Schema({
  desde: Date,
  hasta: Date,
  arbol: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Arbol'
  }, 
  usuario: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Usuario'
  }
});

//metodo para los dias de reserva
reservaSchema.methods.diasDeReserva = function() {
  return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
}

module.exports = mongoose.model('Reserva', reservaSchema);